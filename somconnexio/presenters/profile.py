

class SponsorshipInformation:
    def __init__(self, partner):
        self.partner = partner

    def sponsorship_code(self):
        return self.partner.sponsorship_code

    def sponsees_number(self):
        return self.partner.sponsees_number

    def sponsees_max(self):
        return self.partner.sponsees_max


class Profile:
    def __init__(self, current_user, partner, customer, keycloak_user):
        self.current_user = current_user
        self.partner = partner
        self.customer = customer
        self.keycloak_user = keycloak_user

    def full_name(self):
        return self.partner.name

    def first_name(self):
        return self.partner.firstname

    def username(self):
        return self.current_user.username

    def preferred_locale(self):
        return self.current_user.profile.preferredLocale

    def email(self):
        return self.keycloak_user["email"]

    def phone(self):
        return self.partner.phone

    def sponsorship_information(self):
        return SponsorshipInformation(self.partner)

    @property
    def role(self):
        if self.partner.member or self.partner.coop_candidate:
            return "member"
        elif self.partner.sponsor_id:
            return "sponsored"
        elif self.partner.coop_agreement_code:
            return "coop_agreement"
