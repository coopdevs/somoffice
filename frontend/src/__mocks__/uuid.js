let counter = 0;
let prefix = "";

const v4 = () => prefix + (counter++);
v4.reset = (val = 0) => (counter = val);
v4.setPrefix = (val = "") => (prefix = val);

module.exports = { v4 };
