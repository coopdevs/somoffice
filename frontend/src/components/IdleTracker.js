import React, { useState, useEffect } from "react";
import { Modal } from "components/Modal";
import { Button } from "components/Button";
import { Tiles } from "components/layouts/Tiles";
import { useIdle } from "react-use";
import { useStore } from "hooks/useStore";
import { useApplicationContext } from "hooks/useApplicationContext";
import { getProfile } from "lib/api/profile";

const minutesToMs = minutes => minutes * 60 * 1000;
const IDLE_TIME_MS = minutesToMs(10);

export const IdleTracker = () => {
  const isIdle = useIdle(IDLE_TIME_MS);
  const [isOpen, setIsOpen] = useState(null);
  const { currentUser } = useApplicationContext();
  const isSessionExpired = useStore(state => state.sessionExpired);

  useEffect(() => {
    if (!currentUser) {
      return;
    }

    if (isIdle) {
      setIsOpen(true);
    }
  }, [isIdle, currentUser]);

  if (isSessionExpired) {
    return null;
  }

  const reactivateSession = async () => {
    try {
      await getProfile({ promptSessionExpired: true });
    } catch (e) {
      console.error(e);
    }
    setIsOpen(false);
  };

  return (
    <>
      <Modal
        isOpen={isOpen}
        title="You are idle"
        onClose={() => setIsOpen(false)}
        onConfirm={() => setIsOpen(false)}
      >
        <Tiles columns={1}>
          <p>You have been idle for a while. You will be logged out shortly.</p>
          <Button onClick={reactivateSession}>Continuar usando</Button>
        </Tiles>
      </Modal>
    </>
  );
};
