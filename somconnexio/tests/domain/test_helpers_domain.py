from somconnexio.domain.helpers import get_lastName


class TestCaseHelpersDomain:
    def test_get_lastName(self):
        name = 'Mora, Ortiz'
        assert get_lastName(name) == 'Ortiz'

        name = 'Melina'
        assert get_lastName(name) == ''

