from somconnexio.domain.keycloak import KeycloakRemoteUserBackend
from somconnexio.providers.ticket_types.base_ticket import BaseTicket


class BaseCustomerTicket(BaseTicket):
    TICKET_TYPE = "Petición"

    def _get_customer_code(self):
        keycloak_user = KeycloakRemoteUserBackend().get_keycloak_user(self.user.username)

        if not keycloak_user:
            raise Exception(
                "Error sending email. Cannot find keycloak user for %s." % self.user.username
            )

        customer_code = self.user.profile.remoteId
        return customer_code

    def _get_process_management_dynamic_fields(self):
        result = {
            "ProcessManagementProcessID": self._get_process_id(),
            "ProcessManagementActivityID": self._get_activity_id(),
        }

        for k, v in dict(result).items():
            if v is None:
                del result[k]

        return result

    def _get_activity_id(self):
        return None

    def _get_process_id(self):
        return None
