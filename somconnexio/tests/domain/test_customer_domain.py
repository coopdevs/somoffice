import pytest
from somconnexio.domain.customer import get_customer_resource

from pyopencell.exceptions import PyOpenCellAPIException


class TestCaseCustomerDomain:
    def test_get_customer_resource_ok(self, mocker):
        customer_code = '123'
        expected_customer = object

        CustomerMock = mocker.patch(
            'somconnexio.domain.customer.Customer',
            spec_set=['get']
        )
        CustomerMock.get.return_value = mocker.Mock(spec_set=['customer'])
        CustomerMock.get.return_value.customer = expected_customer

        customer = get_customer_resource(customer_code)

        assert customer == expected_customer
        CustomerMock.get.assert_called_once_with(customer_code)

    def test_get_customer_resource_None(self, mocker):
        customer_code = '123'
        expected_customer = None

        CustomerMock = mocker.patch(
            'somconnexio.domain.customer.Customer',
            spec_set=['get']
        )
        CustomerMock.get.side_effect = PyOpenCellAPIException(None, None, None, None)

        customer = get_customer_resource(customer_code)

        assert customer == expected_customer
        CustomerMock.get.assert_called_once_with(customer_code)
