import React, { useState, useEffect } from "react";
import { SidebarLayout } from "components/layouts/SidebarLayout";
import { Trans, useTranslation } from "react-i18next";
import { theme } from "../theme";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { CircularGraph } from "components/CircularGraph";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import { Tiles } from "components/layouts/Tiles";
import { Text } from "components/Text";
import Grid from "@material-ui/core/Grid";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import WhatsAppIcon from "@material-ui/icons/WhatsApp";
import TelegramIcon from "@material-ui/icons/Telegram";
import { IconButton } from "components/IconButton";
import { useApplicationContext } from "hooks/useApplicationContext";
import { useCopyToClipboard } from "react-use";

const useStyles = makeStyles((theme) => ({
  codeToSponsor: {
    backgroundColor: theme.palette.background.dark,
    fontWeight: 600,
    padding: theme.spacing(1, 2),
  },
}));

function TextDescriptionSponsoreds({ sponseesNumber, sponseesMax }) {
  const { t } = useTranslation();

  const remainingSponsoreds = sponseesMax - sponseesNumber;

  return (
    <Tiles spacing={0} columns={1}>
      <Box position="relative">
        <Typography variant="h4">
          {t("sponsor.have_got", {
            sponseesNumber: sponseesNumber,
            sponseesMax: sponseesMax,
          })}
        </Typography>
      </Box>
      <Box position="relative">
        <Typography variant="h5">{t("sponsor.sponsoreds_persons")}</Typography>
      </Box>
      <Box position="relative">
        <Typography variant="subtitle1" color="textSecondary">
          <Trans
            i18nKey={
              remainingSponsoreds !== 0
                ? "sponsor.there_are_left"
                : "sponsor.there_arent_left"
            }
            count={remainingSponsoreds}
          />
        </Typography>
      </Box>
    </Tiles>
  );
}

function FractionCircularGraph({ sponseesNumber, sponseesMax }) {
  return (
    <Box
      position="absolute"
      display="flex"
      alignItems="center"
      justifyContent="center"
      flexWrap="wrap"
    >
      <Text
        size="3xl"
        bold={true}
        color={theme.palette.primary.main}
        textAlign="right"
      >
        {sponseesNumber}
      </Text>
      <Box pt={3}>
        <Text size="md" semibold={true} color={theme.palette.primary.main}>
          {`/${sponseesMax}`}
        </Text>
      </Box>
    </Box>
  );
}

function LabelCircularGraph({ ...props }) {
  const { t } = useTranslation();
  return (
    <>
      <FractionCircularGraph {...props} />
      <Box pt={9}>
        <Text size="md" color={theme.palette.primary.main} bold={true}>
          {t("sponsor.sponsoreds")}
        </Text>
      </Box>
    </>
  );
}

function StepsToSponsor({ codeToSponsor }) {
  const [isCodeSponsorCopied, setIsCodeSponsorCopied] = useState(false);
  const [state, copyToClipboard] = useCopyToClipboard();
  const { t } = useTranslation();
  const literalPartnerSponsorsMe = t("funnel.join.partner_sponsors_me");
  const { preferred_locale, first_name:userName } = useApplicationContext().currentUser;
  /*TODO
  *
  * remove first_name:userName
  * only for testing in staging
  *
  */
  const getUrlJoinSponsored = (app) => {
    if (process.env.NODE_ENV === "production") {
      if (app === "telegram") {
        return preferred_locale === "es"
          ? "https://somconnexio.coop/apadrina-la-fibra"
          : "https://somconnexio.coop/apadrinalafibra";
      }
      return preferred_locale === "es"
        ? "https://somconnexio.coop/apadrina-fibra"
        : "https://somconnexio.coop/apadrinafibra";
    }
    const protocolSomOffice = window.location.protocol;
    const hostSomOffice = window.location.hostname;
    const portSomOffice = hostSomOffice.startsWith("local") ? ":3000" : "";
    return (
      protocolSomOffice +
      "//" +
      hostSomOffice +
      portSomOffice +
      "/product-picker?opting_for_role=sponsored" +
      "&locale=" +
      preferred_locale
    );
  };

  const getEncodedTextParam = (app) => {
    return encodeURIComponent(
      t("sponsor.message_messenger_service", {
        codeToSponsor: codeToSponsor,
        urlJoinSponsored: getUrlJoinSponsored(app),
      })
    );
  };

  function handleOnClickClipboard(e) {
    e.preventDefault();
    copyToClipboard(codeToSponsor);
    state.error ? setIsCodeSponsorCopied(false) : setIsCodeSponsorCopied(true);
  }

  function handleOnClickWhatsApp(e) {
    e.preventDefault();
    window.open("https://wa.me/?text=" + getEncodedTextParam("whatsApp"));
  }

  function handleOnClickTelegram(e) {
    e.preventDefault();
    window.open("https://t.me/share/url?url= &text=" + getEncodedTextParam("telegram"));
  }

  useEffect(() => {
    if (isCodeSponsorCopied) {
      setTimeout(() => {
        setIsCodeSponsorCopied(false);
      }, 3000);
    }
  }, [isCodeSponsorCopied]);

  return (
    <Stepper orientation="vertical">
      <Step active>
        <StepLabel></StepLabel>
        <StepContent>
          <Grid container>
            <Grid item>
              <Typography>{t("sponsor.steps.first.description")}</Typography>
            </Grid>
            <Grid item>
              <ul>
                <li>
                  <Text bold color="black">
                    {t("sponsor.steps.first.first_action")}
                  </Text>
                </li>
                <li>
                  <Typography>
                    <Trans i18nKey="sponsor.steps.first.second_action" />
                  </Typography>
                </li>
              </ul>
            </Grid>
            <Grid container item direction="column" spacing={2}>
              <Grid item container spacing={2} alignItems="flex-end">
                <Grid item>
                  <Typography variant="h5">
                    {t("sponsor.steps.first.code_to_sponsor")}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant="h4" className={useStyles().codeToSponsor}>
                    {codeToSponsor}
                  </Typography>
                </Grid>
                <Grid item>
                  <IconButton
                    tooltipTitle={t("sponsor.steps.first.code_copied")}
                    onClick={(e) => handleOnClickClipboard(e)}
                    open={isCodeSponsorCopied}
                    disableFocusListener
                    disableHoverListener
                    disableTouchListener
                    placement={"right-end"}
                  >
                    <FileCopyIcon fontSize="large" />
                  </IconButton>
                </Grid>
              </Grid>
              <Grid item>
                <Typography variant="subtitle2" color="textSecondary">
                  {t("sponsor.steps.first.small_print")}
                </Typography>
              </Grid>
              <Grid item container spacing={4} alignItems="center">
                <Grid item>
                  <Typography variant="body2">
                    {t("sponsor.steps.first.share_via_social_media")}
                  </Typography>
                </Grid>
                <Grid item>
                  <IconButton
                    isOutlined
                    onClick={(e) => handleOnClickWhatsApp(e)}
                  >
                    <WhatsAppIcon />
                  </IconButton>
                </Grid>
                <Grid item>
                  <IconButton
                    isOutlined
                    onClick={(e) => handleOnClickTelegram(e)}
                  >
                    <TelegramIcon />
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </StepContent>
      </Step>
      <Step active>
        <StepLabel></StepLabel>
        <StepContent>
          <Typography>
            <Trans i18nKey="sponsor.steps.second.description">
              <Text bold color="black">
                {{ literalPartnerSponsorsMe }}
              </Text>
            </Trans>
          </Typography>
        </StepContent>
      </Step>
    </Stepper>
  );
}

export const Sponsor = () => {
  const { t } = useTranslation();
  const {
    sponsees_number: sponseesNumber,
    sponsees_max: sponseesMax,
    sponsorship_code: sponsorshipCode,
  } = useApplicationContext().currentUser.sponsorship_information;

  return (
    <SidebarLayout>
      <Tiles spacing={4} columns={1}>
        <Typography variant="h4" component="h1" align="left">
          {t("sponsor.description")}
        </Typography>
        <Tiles spacing={0}>
          <Box textAlign="center">
            <CircularGraph
              value={sponseesNumber}
              maxValue={sponseesMax}
              labelGenerator={() =>
                LabelCircularGraph({ sponseesNumber, sponseesMax })
              }
              marginBottomLabel={40}
            />
          </Box>
          <Box pt={4}>
            <TextDescriptionSponsoreds
              sponseesNumber={sponseesNumber}
              sponseesMax={sponseesMax}
            />
          </Box>
        </Tiles>
        {sponseesMax !== sponseesNumber && (
          <Box textAlign="left">
            <StepsToSponsor codeToSponsor={sponsorshipCode} />
          </Box>
        )}
      </Tiles>
    </SidebarLayout>
  );
};
