import pytest

from somconnexio.providers.ticket_types.report_error_ticket import ReportErrorTicket


class TestCaseErrorTicket:
    def test_create_with_user(self, mocker, current_user_fixture):
        OTRSClientMock = mocker.patch(
            "somconnexio.providers.ticket_types.base_ticket.OTRSClient",
            return_value=mocker.Mock()
        )
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())
        expected_ticket_data = {
            "Title": "Error submit",
            "Queue": "Sistemes::Error formularis",
            "State": ReportErrorTicket.TICKET_STATE,
            "Type": ReportErrorTicket.TICKET_TYPE,
            "Priority": ReportErrorTicket.TICKET_PRIORITY,
            "CustomerUser": "sistemes@somconnexio.coop",
            "CustomerID": "sistemes@somconnexio.coop"
        }
        expected_message = """

            Se ha producido un error al intentar realizar el submit en los formularios con los siguientes datos:

            {
    "error": 400
}


            Con el usuario con ref: 1234
"""
        expected_article_data = {
            "Subject": "Error submit",
            "Body": expected_message
        }

        fields_dict = {
            "request": {"error": 400},
            "user": current_user_fixture,
        }

        ReportErrorTicket(current_user_fixture, fields_dict).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)
        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[]
        )

    def test_create_without_user(self, mocker):
        OTRSClientMock = mocker.patch(
            "somconnexio.providers.ticket_types.base_ticket.OTRSClient",
            return_value=mocker.Mock()
        )
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())

        expected_ticket_data = {
            "Title": "Error submit",
            "Queue": "Sistemes::Error formularis",
            "State": ReportErrorTicket.TICKET_STATE,
            "Type": ReportErrorTicket.TICKET_TYPE,
            "Priority": ReportErrorTicket.TICKET_PRIORITY,
            "CustomerUser": "sistemes@somconnexio.coop",
            "CustomerID": "sistemes@somconnexio.coop"
        }
        expected_message = """

            Se ha producido un error al intentar realizar el submit en los formularios con los siguientes datos:

            {
    "error": 400
}
"""
        expected_article_data = {
            "Subject": "Error submit",
            "Body": expected_message
        }

        user = None

        fields_dict = {
            "request": {"error": 400},
            "user": user,
        }

        ReportErrorTicket(user, fields_dict).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)
        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[]
        )
