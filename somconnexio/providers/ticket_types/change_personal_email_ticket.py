import uuid

from somconnexio.providers.ticket_types.base_change_email_ticket import \
    BaseChangeEmailTicket


class ChangePersonalEmailTicket(BaseChangeEmailTicket):
    def _get_queue(self):
        return "Oficina Virtual::Canvi email::Rebut"

    def _get_subject(self):
        return "Canvi d'email dades personals OV"

    def _get_dynamic_fields(self):
        return {
            "flagPartner": True,
            "IDOV": str(uuid.uuid4()),
            "nouEmail": self.fields["email"],
        }
