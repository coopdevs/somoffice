from somconnexio.presenters.profile import Profile


def test_profile_presenter(
        current_user_fixture,
        partner_fixture,
        keycloak_user_fixture,
        ):
    customer = object

    profile = Profile(
        current_user_fixture,
        partner_fixture,
        customer,
        keycloak_user_fixture,
    )

    sponsor_info = profile.sponsorship_information()

    assert profile.full_name() == "Name Lastname"
    assert profile.first_name() == "Name"
    assert profile.username() == "username"
    assert profile.preferred_locale() == "ca"
    assert profile.email() == "fake@email.coop"
    assert profile.phone() == "666666666"
    assert profile.role == "member"
    assert profile.customer == customer
    assert sponsor_info.sponsorship_code() == "jblbp"
    assert sponsor_info.sponsees_number() == 3
    assert sponsor_info.sponsees_max() == 5
