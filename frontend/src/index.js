import "react-app-polyfill/stable";
import "initializers/initializeBugsnag";
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { BrowserRouter as Router } from "react-router-dom";
import Bugsnag from "@bugsnag/js";

/**
 * In production we wrap everything into bugsnag error boundary. In development
 * we just render children, to follow normal create-react-app development flow.
 */
const getAppWrapper = () => {
  if (process.env.NODE_ENV === "production") {
    return Bugsnag.getPlugin("react").createErrorBoundary(React);
  } else {
    return ({ children }) => <>{children}</>;
  }
};

const AppWrapper = getAppWrapper();

ReactDOM.render(
  <AppWrapper>
    <Router>
      <App />
    </Router>
  </AppWrapper>,
  document.getElementById("root")
);
