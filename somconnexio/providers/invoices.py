from pyopencell.resources.invoice_list import InvoiceList
from pyopencell.client import Client
import tempfile
import codecs

from somoffice.core.secure_resources import build_secure_id_generator
from somconnexio.serializers.invoices import InvoicesSerializer
from somconnexio.domain.customer import billing_accounts_for

# pyopencell limit default limit is 10, we raise it *a little bit* here to
# avoid invoices being omitted in the final response
#
# since in general invoices are generated monthly it should be safe to ask for
# a lot of invoices while we implement filtering/pagination in a
# provider-agnostic way in somoffice
LOTS_OF_INVOICES = 500


class InvoicesProvider:
    def __init__(self, options):
        self.options = options
        self.secure_id_generator = build_secure_id_generator(
            user_id=options["remote_user_id"], resource_type="invoice"
        )

    def get_resources(self):
        invoices = []
        customer_code = self.options["remote_user_id"]

        for billing_account in billing_accounts_for(customer_code):
            invoices += InvoiceList.get(
                query="billingAccount.code:{}".format(billing_account["code"]),
                limit=LOTS_OF_INVOICES,
            ).invoices

        serializer = InvoicesSerializer(
            {"secure_id_generator": self.secure_id_generator}
        )

        return serializer.serialize(invoices)

    def is_invoice_ready_to_download(self, resource_id):
        invoice_response = Client().get("/invoice", id=resource_id, includePdf=True)

        if "pdf" not in invoice_response["invoice"]:
            return False
        else:
            return True

    def download_invoice(self, resource_id):
        invoice_response = Client().get("/invoice", id=resource_id, includePdf=True)

        if "pdf" not in invoice_response["invoice"]:
            return None

        invoice_base64 = invoice_response["invoice"]["pdf"]

        # TODO use django NamedTemporaryFile?
        fp = tempfile.NamedTemporaryFile(delete=False)
        fp.write(codecs.decode(bytes(invoice_base64, "utf8"), "base64"))
        fp.close()

        return fp.name
