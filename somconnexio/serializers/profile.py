from rest_framework import serializers


class SponsorshipInfoSerializer(serializers.Serializer):
    sponsorship_code = serializers.CharField()
    sponsees_number = serializers.IntegerField()
    sponsees_max = serializers.IntegerField()


class ProfileSerializer(serializers.Serializer):
    username = serializers.CharField()
    full_name = serializers.CharField()
    first_name = serializers.CharField()
    preferred_locale = serializers.CharField()
    email = serializers.CharField()
    role = serializers.CharField()
    phone = serializers.CharField()
    sponsorship_information = SponsorshipInfoSerializer(required=False)
