# How to run an interactive console

If you need to run an interactive python console to debug problems, run the following commands:

```sh
# development

ssh somoffice@local.somoffice.coop
cd /opt/somoffice
export $(cat /etc/default/somoffice | grep -v '^#' | xargs)
poetry run python manage.py shell

# staging

ssh <your-user>@somoffice.coopdevs.org
sudo su somoffice
cd /opt/somoffice/current
export $(cat /etc/default/somoffice | grep -v '^#' | xargs)
poetry run python manage.py shell

# production

ssh <your-user>@oficinavirtual.somconnexio.coop
sudo su somoffice
cd /opt/somoffice/current
export $(cat /etc/default/somoffice | grep -v '^#' | xargs)
poetry run python manage.py shell
```

# Execute a script inside the Django shell

```sh
export $(cat /etc/default/somoffice | grep -v '^#' | xargs)
poetry run python manage.py shell < my-script.py
```

# Some snipppets

## Getting keycloak user from vat number

```python
from keycloak import KeycloakOpenID, KeycloakAdmin
from keycloak.exceptions import KeycloakGetError, KeycloakAuthenticationError
from django.conf import settings

keycloak_admin = KeycloakAdmin(
    server_url=settings.KEYCLOAK_SERVER_URL + "/auth/",
    username=settings.KEYCLOAK_ADMIN_USER,
    password=settings.KEYCLOAK_ADMIN_PASSWORD,
    realm_name=settings.KEYCLOAK_REALM,
    verify=True,
)

vat_number = "es12341234x"
users = keycloak_admin.get_users({ "username": vat_number })
user = users[0]
```

## Finding an user in somoffice db

```python
from somoffice.models import User, Profile
vat_number = "es12341234x"
user = User.objects.get(username=vat_number)

# You can check the locale
user.profile.preferredLocale

# Or the customer code
user.profile.remoteId
```
