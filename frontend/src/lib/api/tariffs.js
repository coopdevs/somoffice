import { get } from "axios";

/**
 * TODO the field `has_landline_phone` should come from product catalog, we are faking it here meanwhile
 */
const decorateTarrifs = tariff => {
  return {
    ...tariff,
    has_landline_phone:
      tariff.category === "fiber" && tariff.code !== "SE_SC_REC_BA_F_100_SF"
  };
};

export const getTariffs = async ({ code = "21IVA" } = {}) => {
  const { data } = await get(`/api/product-catalog/`, {
    params: { code },
  });

  try {
    return data[0].products.map(decorateTarrifs);
  } catch (e) {
    console.error(e);
    /* handle error */
  }
};
