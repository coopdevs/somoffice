# Generated by Django 2.2.17 on 2021-02-03 11:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('somoffice', '0004_emailconfirmationtoken'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emailconfirmationtoken',
            name='expiresAt',
            field=models.DateTimeField(),
        ),
    ]
