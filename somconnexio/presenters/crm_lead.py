class CRMLeadPresenter:
    """
    Returns a dict with the data needed to create CrmLead object in Odoo.
    """
    def __init__(self, services_form, partner_id, sr_id):
        self.services_form = services_form
        self.partner_id = partner_id
        self.sr_id = sr_id

    def get_mobile_isp_info(self, raw_data):
        data = self._remove_empty_values_from_dict(raw_data)
        self.set_previous_owner_data(data)

        return data

    def get_broadband_isp_info(self, raw_data):
        data = self._remove_empty_values_from_dict(raw_data)
        self.set_previous_owner_data(data)

        return data

    def get_crm_lead_lines(self, line):
        line_data = self._remove_empty_values_from_dict(line)
        if "mobile_isp_info" in line_data:
            line_data["mobile_isp_info"] = self.get_mobile_isp_info(
                line_data["mobile_isp_info"]
            )
        elif "broadband_isp_info" in line_data:
            line_data["broadband_isp_info"] = self.get_broadband_isp_info(line_data["broadband_isp_info"])

        return line_data

    def get_crm_lead(self):
        iban = (
            self.services_form.get("iban") or
            self.services_form.get("service_iban")
        )
        crm_lead_lines = []
        for line in self.services_form.get("lines"):
            data = {
                  "partner_id": self.partner_id,
                  "subscription_request_id": self.sr_id,
                  "iban": iban,
                  "lead_line_ids": [self.get_crm_lead_lines(line)],
            }
            data = self._remove_empty_values_from_dict(data)
            crm_lead_lines.append(data)
        return crm_lead_lines

    def set_previous_owner_data(self, data):
        if data.get("previous_owner_name"):
            data["previous_owner_first_name"] = data["previous_owner_name"]
            if data.get("previous_owner_surname"):
                previous_owner_name = "{} {}".format(
                    data["previous_owner_surname"],
                    data["previous_owner_lastname"]
                )
            else:
                previous_owner_name = data.get("previous_owner_lastname")
            if data.get("previous_owner_vat"):
                data["previous_owner_vat_number"] = data["previous_owner_vat"]

            data["previous_owner_name"] = previous_owner_name

    def _remove_empty_values_from_dict(self, dict):
        return {
            x: y
            for x, y in dict.items()
            if y is not None
        }
