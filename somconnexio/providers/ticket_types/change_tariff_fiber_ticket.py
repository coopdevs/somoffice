from somconnexio.providers.ticket_types.base_change_tariff_ba_ticket import (
    BaseChangeTariffTicketBA,
)


class ChangeTariffTicketFiber(BaseChangeTariffTicketBA):
    def _get_process_id(self):
        return "Process-a7b08ec0f281167899a5b72c8bd0899d"

    def _get_activity_id(self):
        return "Activity-ea6172a702d09d7cbe8c2b44de9a9853"

    def _get_subject(self):
        return "Sol·licitud Canvi de tarifa sense fix oficina virtual"

    def _get_queue(self):
        return "Oficina Virtual::Canvi Tarifa BA::Fibra a Fibra SF::Rebut"

    def _get_specfic_dynamic_fields(self):
        return {
            "serveiPrevi": "Fibra",
            "dataExecucioCanviTarifa": self.fields["effective_date"],
        }
