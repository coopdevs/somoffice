from otrs_somconnexio.client import OTRSClient

# TODO: Move the usage of PyOTRS to OTRS_SomConnexio
from pyotrs.lib import DynamicField

from somoffice.core.secure_resources import build_secure_id_generator, validate_secure_id

from somconnexio.providers.ticket_types.report_error_ticket import ReportErrorTicket
from somconnexio.providers.ticket_types.change_iban_ticket import ChangeIbanTicket
from somconnexio.providers.ticket_types.change_email_ticket import ChangeEmailTicket
from somconnexio.providers.ticket_types.add_data_ticket import AddDataTicket
from somconnexio.providers.ticket_types.change_tariff_ticket import ChangeTariffTicket
from somconnexio.providers.ticket_types.change_personal_email_ticket import ChangePersonalEmailTicket
from somconnexio.providers.ticket_types.change_tariff_adsl_ticket import ChangeTariffTicketADSL
from somconnexio.providers.ticket_types.change_tariff_fiber_ticket import ChangeTariffTicketFiber


def build_ticket(ticket_type, user, fields, override_ticket_ids=[]):
    if ticket_type == "additional_data":
        TicketConstructor = AddDataTicket
    elif ticket_type == "change_tariff":
        TicketConstructor = ChangeTariffTicket
    elif ticket_type == "change_tariff_out_landline":
        if fields["previous_service"] == "SE_SC_REC_BA_ADSL_SF":
            TicketConstructor = ChangeTariffTicketADSL
        else:
            TicketConstructor = ChangeTariffTicketFiber
    elif ticket_type == "change_email":
        TicketConstructor = ChangeEmailTicket
    elif ticket_type == "change_personal_email":
        TicketConstructor = ChangePersonalEmailTicket
    elif ticket_type == "change_iban":
        TicketConstructor = ChangeIbanTicket
    elif ticket_type == "report_error":
        TicketConstructor = ReportErrorTicket
    else:
        raise ValueError("Unknown ticket type: %s" % ticket_type)

    return TicketConstructor(user, fields, override_ticket_ids)


class TicketsProvider:

    def __init__(self, options={}):
        self.options = options
        self.errors = []
        self.client = OTRSClient()

    def create(self, user, meta, override_ticket_ids=[]):
        meta_dict = self._build_meta_dict(meta)

        decoded_ticket_ids = [validate_secure_id(
            user, id)["resource_id"] for id in override_ticket_ids]

        if None in decoded_ticket_ids:
            raise ValueError("Invalid ticket id")

        for ticket_id in decoded_ticket_ids:
            self.client.client.ticket_update(
                ticket_id,
                State="closed successful"
            )

        ticket = build_ticket(
            meta_dict["ticket_type"], user, meta_dict, override_ticket_ids)

        ticket.create()

        return True

    def get_resources(self, ticket_type, filters):
        user = self.options["user"]
        remote_user_id = user.profile.remoteId
        secure_id_generator = build_secure_id_generator(
            user_id=remote_user_id, resource_type="ticket"
        )

        reference_ticket = build_ticket(ticket_type, user, {})
        search_args = reference_ticket.get_search_args()

        for filter_tuple in filters:
            name, value = filter_tuple
            search_args["dynamic_fields"].append(
                DynamicField(name, search_patterns=[value]),
            )

        tickets = self.client.search_tickets(
            **search_args, CustomerID=self.options["user"].profile.remoteId)

        return [{"id": secure_id_generator(ticket.id), "meta": [{"key": "new_product_code", "value": ticket.response.dynamic_field_get("productMobil").value}]} for ticket in tickets]

    # TODO: Remove :fire:
    def is_valid():
        return len(self.errors) == 0

    def _build_meta_dict(self, meta):
        result = {}

        for item in meta:
            result[item["key"]] = item["value"]

        return result
