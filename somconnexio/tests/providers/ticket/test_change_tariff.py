import pytest

from pyotrs.lib import DynamicField

from somconnexio.providers.ticket_types.change_tariff_ticket import ChangeTariffTicket


class TestCaseChangeTariffTicket:
    def test_create(self, mocker, current_user_fixture):
        OTRSClientMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.OTRSClient", return_value=mocker.Mock())
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())
        DynamicFieldMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.DynamicField", return_value=mocker.Mock())
        KeycloakRemoteUserBackendMock = mocker.patch("somconnexio.providers.ticket_types.base_customer_ticket.KeycloakRemoteUserBackend", return_value=mocker.Mock())
        translationMock = mocker.patch("somconnexio.providers.ticket_types.change_tariff_ticket.translation", return_value=mocker.Mock())
        translationMock.get_language.return_value = "ca"

        expected_ticket_data = {
            "Title": "Sol·licitud Canvi de tarifa oficina virtual",
            "Queue": "Oficina Virtual::Canvi Tarifa mòbil::Rebut",
            "State": ChangeTariffTicket.TICKET_STATE,
            "Type": ChangeTariffTicket.TICKET_TYPE,
            "Priority": ChangeTariffTicket.TICKET_PRIORITY,
            "CustomerUser": current_user_fixture.profile.remoteId,
            "CustomerID": current_user_fixture.profile.remoteId
        }
        expected_article_data = {
            "Subject": "Sol·licitud Canvi de tarifa oficina virtual",
            "Body": "-"
        }

        fields_dict = {
            "phone_number": "666666666",
            "new_product_code": "NEW_PRODUCT_CODE",
            "current_product_code": "CURRENT_PRODUCT_CODE",
            "effective_date": "tomorrow",
            "subscription_email": "fakeemail@email.coop",
        }

        ChangeTariffTicket(current_user_fixture, fields_dict).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)
        calls = [
            mocker.call('ProcessManagementProcessID', 'Process-f91240baa6e0146aecc70a9c97d6f84f'),
            mocker.call('ProcessManagementActivityID', 'Activity-7117b19116339f88dc43767cd477f2be'),
            mocker.call('renovaCanviTarifa', False),
            mocker.call('liniaMobil', '666666666'),
            mocker.call('productMobil', 'NEW_PRODUCT_CODE'),
            mocker.call('tarifaAntiga', 'CURRENT_PRODUCT_CODE'),
            mocker.call('dataExecucioCanviTarifa', 'tomorrow'),
            mocker.call('correuElectronic', 'fakeemail@email.coop'),
            mocker.call('idioma', 'ca_ES')
        ]
        DynamicFieldMock.assert_has_calls(calls)
        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[mocker.ANY for call in calls]
        )

    def test_create_with_override_tickets(self, mocker, current_user_fixture):
        OTRSClientMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.OTRSClient", return_value=mocker.Mock())
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())
        DynamicFieldMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.DynamicField", return_value=mocker.Mock())
        KeycloakRemoteUserBackendMock = mocker.patch("somconnexio.providers.ticket_types.base_customer_ticket.KeycloakRemoteUserBackend", return_value=mocker.Mock())
        translationMock = mocker.patch("somconnexio.providers.ticket_types.change_tariff_ticket.translation", return_value=mocker.Mock())
        translationMock.get_language.return_value = "ca"

        expected_ticket_data = {
            "Title": "Sol·licitud Canvi de tarifa oficina virtual",
            "Queue": "Oficina Virtual::Canvi Tarifa mòbil::Rebut",
            "State": ChangeTariffTicket.TICKET_STATE,
            "Type": ChangeTariffTicket.TICKET_TYPE,
            "Priority": ChangeTariffTicket.TICKET_PRIORITY,
            "CustomerUser": current_user_fixture.profile.remoteId,
            "CustomerID": current_user_fixture.profile.remoteId
        }
        expected_article_data = {
            "Subject": "Sol·licitud Canvi de tarifa oficina virtual",
            "Body": "-"
        }

        fields_dict = {
            "phone_number": "666666666",
            "new_product_code": "NEW_PRODUCT_CODE",
            "current_product_code": "CURRENT_PRODUCT_CODE",
            "effective_date": "tomorrow",
            "subscription_email": "fakeemail@email.coop",
        }

        ChangeTariffTicket(current_user_fixture, fields_dict, override_ticket_ids=[1,2]).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)
        calls = [
            mocker.call('ProcessManagementProcessID', 'Process-f91240baa6e0146aecc70a9c97d6f84f'),
            mocker.call('ProcessManagementActivityID', 'Activity-7117b19116339f88dc43767cd477f2be'),
            mocker.call('renovaCanviTarifa', True),
            mocker.call('liniaMobil', '666666666'),
            mocker.call('productMobil', 'NEW_PRODUCT_CODE'),
            mocker.call('tarifaAntiga', 'CURRENT_PRODUCT_CODE'),
            mocker.call('dataExecucioCanviTarifa', 'tomorrow'),
            mocker.call('correuElectronic', 'fakeemail@email.coop'),
            mocker.call('idioma', 'ca_ES')
        ]
        DynamicFieldMock.assert_has_calls(calls)
        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[mocker.ANY for call in calls]
        )
