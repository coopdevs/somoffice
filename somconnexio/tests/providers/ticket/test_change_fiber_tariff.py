import pytest

from pyotrs.lib import DynamicField

from somconnexio.providers.ticket_types.change_tariff_fiber_ticket import ChangeTariffTicketFiber


class TestCaseChangeFiberTariffTicket:
    def test_create(self, mocker, current_user_fixture):
        OTRSClientMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.OTRSClient", return_value=mocker.Mock())
        TicketMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Ticket", return_value=mocker.Mock())
        ArticleMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.Article", return_value=mocker.Mock())
        DynamicFieldMock = mocker.patch("somconnexio.providers.ticket_types.base_ticket.DynamicField", return_value=mocker.Mock())
        KeycloakRemoteUserBackendMock = mocker.patch("somconnexio.providers.ticket_types.base_customer_ticket.KeycloakRemoteUserBackend", return_value=mocker.Mock())

        expected_ticket_data = {
            "Title": "Sol·licitud Canvi de tarifa sense fix oficina virtual",
            "Queue": "Oficina Virtual::Canvi Tarifa BA::Fibra a Fibra SF::Rebut",
            "State": ChangeTariffTicketFiber.TICKET_STATE,
            "Type": ChangeTariffTicketFiber.TICKET_TYPE,
            "Priority": ChangeTariffTicketFiber.TICKET_PRIORITY,
            "CustomerUser": current_user_fixture.profile.remoteId,
            "CustomerID": current_user_fixture.profile.remoteId
        }
        expected_article_data = {
            "Subject": "Sol·licitud Canvi de tarifa sense fix oficina virtual",
            "Body": "-"
        }

        fields_dict = {
            "ref_contract": "1256",
            "landline_number": "969999999",
            "service_address": "Carrer",
            "service_city": "Ciutat",
            "service_zip": "ZIPCODE",
            "service_state": "State",
            "effective_date": "1931-04-14"
        }

        ChangeTariffTicketFiber(current_user_fixture, fields_dict).create()

        TicketMock.assert_called_once_with(expected_ticket_data)
        ArticleMock.assert_called_once_with(expected_article_data)
        calls = [
                mocker.call('ProcessManagementProcessID', 'Process-a7b08ec0f281167899a5b72c8bd0899d'),
                mocker.call('ProcessManagementActivityID', 'Activity-ea6172a702d09d7cbe8c2b44de9a9853'),
                mocker.call('refOdooContract', '1256'),
                mocker.call('telefonFixVell', '969999999'),
                mocker.call('direccioServei', 'Carrer'),
                mocker.call('poblacioServei', 'Ciutat'),
                mocker.call('CPservei', 'ZIPCODE'),
                mocker.call('provinciaServei', 'State'),
                mocker.call('productBA', 'SE_SC_REC_BA_F_100_SF'),
                mocker.call('serveiPrevi', 'Fibra'),
                mocker.call('dataExecucioCanviTarifa', '1931-04-14')
        ]
        DynamicFieldMock.assert_has_calls(calls)
        OTRSClientMock.return_value.client.ticket_create.assert_called_once_with(  # noqa
            TicketMock.return_value,
            article=ArticleMock.return_value,
            dynamic_fields=[mocker.ANY for call in calls]
        )
